class Object3D
{
public:

	GLuint vArray, vBuffer, iBuffer;
	GLuint shaders;
	GLuint texture;
	GLuint normalMap;
	int indexCount;

	void draw();
	void loadObject(string fileName);
	void loadTexture(string textureName, GLuint &texId);
	
	void enableArrays();
	void disableArrays();
};

void Object3D::draw()
{
	enableArrays();
	
	glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, (void *)0);

	disableArrays();	
}

void Object3D::enableArrays()
{
	glBindVertexArray(vArray);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture);
	glActiveTexture(GL_TEXTURE0+1);
	glBindTexture(GL_TEXTURE_2D, normalMap);
}

void Object3D::disableArrays()
{
	glBindVertexArray(0);
	glActiveTexture(GL_TEXTURE0+1);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Object3D::loadObject(string fileName)
{
	glGenVertexArrays(1, &vArray);
	glGenBuffers(1, &vBuffer);
	glGenBuffers(1, &iBuffer);

	
	ObjData object;
	loadObjFile(fileName, &object);
	if(object.textureFileName != "")
		loadTexture(object.textureFileName, texture);
	if(object.normalMapFileName != "")
		loadTexture(object.normalMapFileName, normalMap);

	
	glBindVertexArray(vArray);
	
	glBindBuffer(GL_ARRAY_BUFFER, vBuffer);
	glBufferData(GL_ARRAY_BUFFER, object.data.size()*sizeof(float), object.data.data(), GL_STATIC_DRAW);

	glVertexAttribPointer(glGetAttribLocation(shaders, "position"), 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void *)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(glGetAttribLocation(shaders, "uvCoordinate"), 2, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void *)(3*sizeof(float)));
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(glGetAttribLocation(shaders, "normal"), 3, GL_FLOAT, GL_FALSE, 8*sizeof(float), (void *)(5*sizeof(float)));
	glEnableVertexAttribArray(2);


	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, object.faces.size()*sizeof(float), object.faces.data(), GL_STATIC_DRAW);

	indexCount = object.faces.size();

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
//////////////////////////////////////////////////////////

void Object3D::loadTexture(string textureName, GLuint &texId)
{
	glGenTextures(1, &texId);
	
	int w, h, n;
	unsigned char *image = stbi_load(textureName.c_str(), &w, &h, &n, 0);
	
	glBindTexture(GL_TEXTURE_2D, texId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	stbi_image_free(image);
}
