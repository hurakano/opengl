#include <GL/glew.h>
#include <imgui.h>
#include <imgui-SFML.h>
#include <SFML/Graphics.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>

#include "glem.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define PI 3.14159265359


using namespace sf;
using namespace std;
using namespace glm;

#include "Object.hpp"

int main()
{
//make bindow
	ContextSettings settings;
	settings.depthBits = 24;
	settings.majorVersion = 3;
	settings.minorVersion = 3;
	settings.attributeFlags = ContextSettings::Core;
	RenderWindow window(VideoMode(700, 700), "okno", Style::Default, settings);

	glewExperimental=GL_TRUE;
	glewInit();
	ImGui::SFML::Init(window);

//some variables

	float rotX = 0, rotY = 0;
	float scal = 0.5;
	float speed = 0.1; 
	
	vec3 cameraPosition(0, 2, 0);
	float cameraHorizontalRotation = 0;
	float cameraVerticalRotation = 0;
	float moveMultipiler = 0.01;

//init config
	
	GLuint shaders = loadShaders("vertexShader.glsl", "fragmentShader.glsl");
	
	glUseProgram(shaders);
	glUniform1i(glGetUniformLocation(shaders, "texture1"), 0);
	glUniform1i(glGetUniformLocation(shaders, "texture2"), 1);
	glUseProgram(0);
	
//load objects

	
//object 1

	Object3D scnObj, plane;
	scnObj.shaders = shaders;
	scnObj.loadObject("cottage.obj");
	plane.shaders = shaders;
	plane.loadObject("plane.obj");

	mat4 planeMatrix(1.);
	planeMatrix = scale(planeMatrix, vec3(100, 1, 100));
	
	vector<mat4> modelMatrix;

	for(int i = 0; i < 10; i++)
	{
		for(int j = 0; j < 10; j++)
		{
			mat4 model(1.);
			model = translate(model, vec3(i*25, 0, j*15));
			model = scale(model, vec3(0.5, 0.5, 0.5));
			modelMatrix.push_back(model);	
		}
	}


//

	
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);
	
	//mat4 projMatrix = ortho(-5, 5, -5, 5, -5, 5);
	//mat4 projMatrix = ortho(-1, 1, -1, 1, 1, -1);
	mat4 projMatrix = perspective(45.f, 1.f, 0.1f, 100.f);
	
	mat4 viewMatrix;

	
	Clock imGuiClock;
	Clock mouseClock;
	
	while(window.isOpen())
	{
		Event event;
		
		if(window.pollEvent(event))
		{
			ImGui::SFML::ProcessEvent(event);
			
			if(event.type == Event::Closed || event.type == Event::KeyPressed && event.key.code == Keyboard::Escape)
			{
				window.close();
				break;
			}
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::W)
			{
				cameraPosition.z += speed * cos(cameraHorizontalRotation);
				cameraPosition.x += speed * sin(cameraHorizontalRotation);
			}
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::S)
			{
				cameraPosition.z -= speed * cos(cameraHorizontalRotation);
				cameraPosition.x -= speed * sin(cameraHorizontalRotation);
			}
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::A)
			{
				cameraPosition.x += speed * cos(cameraHorizontalRotation);
				cameraPosition.z += speed * -sin(cameraHorizontalRotation);
			}
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::D)
			{
				cameraPosition.x -= speed * cos(cameraHorizontalRotation);
				cameraPosition.z -= speed * -sin(cameraHorizontalRotation);
			}
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::Q)
			{
				cameraPosition.y += speed;
			}
			if(event.type == Event::KeyPressed && event.key.code == Keyboard::E)
			{
				cameraPosition.y -= speed;
			}
		}
		if(mouseClock.getElapsedTime().asSeconds() >= 0.05)
		{
			mouseClock.restart();
			Vector2i moved = Vector2i(350, 350) - Mouse::getPosition(window);
				cameraHorizontalRotation += moved.x * moveMultipiler;
				cameraVerticalRotation += moved.y * moveMultipiler;
				if(cameraHorizontalRotation > 2.*PI)cameraHorizontalRotation -= 2.*PI;
				if(cameraHorizontalRotation < 0)cameraHorizontalRotation += 2.*PI;
				if(cameraVerticalRotation > 2)cameraVerticalRotation = 2;
				if(cameraVerticalRotation < -2)cameraVerticalRotation = -2;
				Mouse::setPosition(Vector2i(350, 350), window);
		}
		
//set view
		vec3 displacmentVec(float(sin(cameraHorizontalRotation)), 
			cameraVerticalRotation, 
			float(cos(cameraHorizontalRotation)));
		viewMatrix = lookAt(cameraPosition, cameraPosition+displacmentVec, vec3(0, 1, 0));
		
		ImGui::SFML::Update(window, imGuiClock.restart());
//set imGui

		ImGui::Begin("Sample window"); // begin window
		ImGui::Text("X %f Y %f Z %f hor %f vert %f", cameraPosition.x, cameraPosition.y, cameraPosition.z, cameraHorizontalRotation, cameraVerticalRotation);
        ImGui::End(); // end window
        
//render
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
		glUseProgram(shaders);
		glUniform3f(glGetUniformLocation(shaders, "eyePosition"), cameraPosition.x, cameraPosition.y, cameraPosition.z);
		glUniformMatrix4fv(glGetUniformLocation(shaders, "modelMatrix"), 
			modelMatrix.size(), GL_FALSE, value_ptr(modelMatrix[0]));
		glUniformMatrix4fv(glGetUniformLocation(shaders, "projectionMatrix"), 
			1, GL_FALSE, value_ptr(projMatrix));
		glUniformMatrix4fv(glGetUniformLocation(shaders, "cameraMatrix"), 
			1, GL_FALSE, value_ptr(viewMatrix));
//draw
		
		scnObj.enableArrays();
		glDrawElementsInstanced(GL_TRIANGLES, scnObj.indexCount, 
			GL_UNSIGNED_INT, (void *)0, 100);
		scnObj.disableArrays();


		glUniformMatrix4fv(glGetUniformLocation(shaders, "modelMatrix"), 
			1, GL_FALSE, value_ptr(planeMatrix));
		plane.draw();
		
		glUseProgram(0);
		
		
		ImGui::SFML::Render();
		
		window.display();
	}
	
	ImGui::SFML::Shutdown();
	
	return 0;
}
//////////////////////////////////////////////////////////////////
