#version 330 core

uniform sampler2D texture1;
uniform sampler2D texture2;
uniform vec3 eyePosition;

in vec2 uvCoord;
in vec3 vertPosition;
in vec3 vertNormal;

//lights
vec3 lightPos = vec3(-1, 1, 1);
vec3 lighColor = vec3(1, 1, 1);

vec3 directLight = vec3(0.5, 1, 0.3);
vec3 directLightColor = vec3(0.5, 0.5, 0.5);

vec4 ambientColor = vec4(0.1, 0.1, 0.1, 1);

vec3 spotLight1Pos = vec3(4, 5, 8);
vec3 spotLight1Col = vec3(1, 0, 0.5);
float spotLight1Angle = 30;
vec3 spotLight1Dir = vec3(0, -1, 0);

vec3 spotLight2Pos = vec3(-10, 5, 8);
vec3 spotLight2Col = vec3(0, 1, 0.5);
float spotLight2Angle = 45;
vec3 spotLight2Dir = vec3(0.2, -0.8, 0);

float specularStrenght = 0.3;


out vec4 fragColor;

vec4 getLight(vec3 position, vec3 color, vec3 normal, int type, 
	float angle, vec3 direction);


void main()
{
	fragColor = texture2D(texture1, uvCoord);
	vec3 normal = vec3(texture2D(texture2, uvCoord));
	if(normal == vec3(0, 0, 0))
	{
		normal = vertNormal;
	}
	normal = normalize(normal);
	
	vec4 colorDir = getLight(directLight, directLightColor, normal, 1, 0, vec3(0));
	vec4 colorPoint = getLight(lightPos, lighColor, normal, 2, 0, vec3(0));
	vec4 colorSpot1 = getLight(spotLight1Pos, spotLight1Col, normal, 3, spotLight1Angle, spotLight1Dir);
	vec4 colorSpot2 = getLight(spotLight2Pos, spotLight2Col, normal, 3, spotLight2Angle, spotLight2Dir);
	

	fragColor *= ambientColor + colorDir + colorPoint + colorSpot1 + colorSpot2;
}
//////////////////////////////////////////////////////////////////

vec4 getLight(vec3 position, vec3 color, vec3 normal, int type, 
	float angle, vec3 direction)
{

	float attenuation = 1.0;
	vec3 surfaceToLight;
	float distanceToLight;
	
	if(type == 1)
	{
		surfaceToLight = normalize(position);
	}
	if(type == 2 || type == 3)
	{
		surfaceToLight = normalize(position - vertPosition);
		distanceToLight = length(position - vertPosition);
		attenuation = 1.0 / (1.0 + 0.01 * pow(distanceToLight, 2));
	}
	if(type == 3)
	{
		direction = normalize(direction);
		vec3 rayDirection = -surfaceToLight;
		float lightToSurfaceAngle = degrees( acos( dot(rayDirection, direction)));
		if(lightToSurfaceAngle > angle)
			attenuation = 0;
	}

//diffuse

	float diffuseCoef = clamp( dot(normal, surfaceToLight), 0, 1);
	vec3 diffuse = color * diffuseCoef;
	
//specular
	vec3 surfaceToCamera = normalize(eyePosition - vertPosition);
	float specularCoef = 0;
	if(diffuseCoef > 0.0)
		specularCoef = pow( max( 0, dot(surfaceToCamera, reflect(-surfaceToLight, normal))), 10);
	vec3 specular = specularCoef * vec3(1, 1, 1) * specularStrenght;
	
	return vec4(attenuation*(diffuse+specular), 1);
}
