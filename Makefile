.PHONY: clean

libs = -lsfml-graphics -lsfml-window -lsfml-system -lGL -lGLEW -lglem

main: main.cpp imgui-master/imgui.o Object.hpp
	g++ -o main main.cpp imgui-master/imgui.o $(libs) -std=c++11 -Iimgui-master -L.
	
imgui-master/imgui.o:
	cd imgui-master && make
	
clean:
	rm main && cd imgui-master && make clean
