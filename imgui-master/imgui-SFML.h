#ifndef IMGUI_SFML_H
#define IMGUI_SFML_H

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Window/Joystick.hpp>

namespace sf
{
    class Event;
    class RenderTarget;
    class RenderWindow;
    class Window;
}

namespace ImGui
{
namespace SFML
{
    void Init(sf::RenderWindow& window, bool loadDefaultFont = true);
    void Init(sf::Window& window, sf::RenderTarget& target, bool loadDefaultFont = true);

    void ProcessEvent(const sf::Event& event);

    void Update(sf::RenderWindow& window, sf::Time dt);
    void Update(sf::Window& window, sf::RenderTarget& target, sf::Time dt);
    void Update(const sf::Vector2i& mousePos, const sf::Vector2f& displaySize, sf::Time dt);

    void Render();

    void Shutdown();


    // joystick functions
    void SetActiveJoystickId(unsigned int joystickId);
    void SetJoytickDPadThreshold(float threshold);
    void SetJoytickLStickThreshold(float threshold);

    void SetJoystickMapping(int action, unsigned int joystickButton);
    void SetDPadXAxis(sf::Joystick::Axis dPadXAxis, bool inverted = false);
    void SetDPadYAxis(sf::Joystick::Axis dPadYAxis, bool inverted = false);
    void SetLStickXAxis(sf::Joystick::Axis lStickXAxis, bool inverted = false);
    void SetLStickYAxis(sf::Joystick::Axis lStickYAxis, bool inverted = false);
}
}

#endif //# IMGUI_SFML_H
