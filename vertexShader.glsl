#version 330 core

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix[100];
uniform mat4 cameraMatrix;

in vec3 position;
in vec2 uvCoordinate;
in vec3 normal;

out vec2 uvCoord;
out vec3 vertPosition;
out vec3 vertNormal;

void main()
{
	mat4 transformationMatrix = projectionMatrix * cameraMatrix * modelMatrix[gl_InstanceID];
	vec4 vertexPosition = vec4(position, 1);
	
	
	gl_Position = transformationMatrix * vertexPosition;
	
	vertPosition = vec3(modelMatrix[gl_InstanceID] * vertexPosition);
	mat3 normalMatrix = transpose(inverse(mat3(modelMatrix[gl_InstanceID])));
    vertNormal = normalize(normalMatrix * normal);

	uvCoord = uvCoordinate;
}
